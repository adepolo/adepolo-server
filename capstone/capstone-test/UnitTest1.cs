﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using capstone.Controllers;
using capstone.Models;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using System.Web.Script.Serialization;
using capstone.Models;
using capstone.Repositories;
using capstone_model;
using System.Collections.Generic;
using System.Linq;

namespace capstone_test
{
    [TestClass]
    public class UnitTest1
    {
        private TestContext testContextInstance;

        /// <summary>
        ///  Gets or sets the test context which provides
        ///  information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [TestMethod]
        public void TestMethod1()
        {
            object location = new Location() { Latitude = 39.7391536f, Longitude = -104.9847034f };

            var json = new JavaScriptSerializer().Serialize(location);


            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] data = encoder.GetBytes(json);

            var request = (HttpWebRequest)WebRequest.Create
                ("http://localhost:2008/tril/elevation/");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;
            request.Expect = "application/json";

            request.GetRequestStream().Write(data, 0, data.Length);

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;

            System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
            string content = reader.ReadToEnd();
            dynamic item = new JavaScriptSerializer().Deserialize<object>(content);

            Assert.AreEqual<float>(item["results"][0]["elevation"],1608.71142578125f);

        }

        [TestMethod]
        public void TestDijk()
        {
            using (var db = new adepoloEntities())
            {
                Graph g = new Graph();
                List<vertex> data = db.vertices.Where(d => d.bridge_name.StartsWith("G") || d.bridge_name.Contains("Strabucks"))
                    .ToList<vertex>();

                /*for (int i = 0; i < data.Count; i++)
                {
                    var connections = new Dictionary<string, int>();

                    if (data[i].north_name != null) connections.Add(data[i].north_name, data[i].north_value.Value);
                    if (data[i].east_name != null) connections.Add(data[i].east_name, data[i].east_value.Value);
                    if (data[i].south_name != null) connections.Add(data[i].south_name, data[i].south_value.Value);
                    if (data[i].west_name != null) connections.Add(data[i].west_name, data[i].west_value.Value);

                    g.add_vertex(data.ElementAt(i).bridge_name, connections);
                }*/

                var start = "";
                var dest = "";
                var all_passes = "";
                var error = "";

                try
                {
                    for (int i = 0; i < data.Count - 1; i++)
                    {
                        for (int j = 0; j < data.Count; j++)
                        {
                            start = data[i].bridge_name;
                            dest = data[j].bridge_name;
                            var op = g.shortest_path(start, dest);
                            op.Add(start);
                            var path = "";
                            foreach(string p in op)
                            {
                                path += p + "...";
                            }
                            all_passes += path;
                            all_passes += "\n";
                        }
                    }
                    TestContext.WriteLine("PASSED: \n" + all_passes);
                }
                catch (Exception ex)
                {
                    error += all_passes + "\n\n";
                    error += "ERROR:   \n" + start;
                    error += "";
                    error += dest;
                    error += " | ";
                    TestContext.WriteLine( error);
                }
            }
        }
    }
}
