﻿namespace capstone.Models
{
    public class RoomParameter
    {
        public string ROOM { get; set; }
        public string LABEL_1 { get; set; }
        public string LABEL_2 { get; set; }
        public string MAC_1 { get; set; }
        public string MAC_2 { get; set; }
        public string MAC_3 { get; set; }
        public string MAC_4 { get; set; }
        public string MAC_5 { get; set; }
    }
}
