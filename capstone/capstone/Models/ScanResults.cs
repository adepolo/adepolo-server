﻿namespace capstone.Models
{
    // this class was created, when the unity map had not been yet introduced.
    // we still love it, that's why it is here
    public class ScanResults
    {
        public const int CHANNEL_WIDTH_20MHZ = 0;
        public const int CHANNEL_WIDTH_40MHZ = 1;
        public const int CHANNEL_WIDTH_80MHZ = 2;
        public const int CHANNEL_WIDTH_160MHZ = 3;
        public const int CHANNEL_WIDTH_80MHZ_PLUS_MHZ = 4;
        // The address of the access point.
        public string BSSID { get; set; }
        // The network name.
        public string SSID { get; set; }
        // Describes the authentication, key management, and encryption schemes supported by the access point.
        public string capabilities { get; set; }
        // Not used if the AP bandwidth is 20 MHz If the AP use 40, 80 or 160 MHz, this is the center frequency (in MHz) 
        // if the AP use 80 + 80 MHz, this is the center frequency of the first segment (in MHz)
        public int centerFreq0 { get; set; }
        // Only used if the AP bandwidth is 80 + 80 MHz if the AP use 80 + 80 MHz,
        // this is the center frequency of the second segment (in MHz)
        public int centerFreq1 { get; set; }
        // AP Channel bandwidth; one of CHANNEL_WIDTH_20MHZ, CHANNEL_WIDTH_40MHZ, 
        // CHANNEL_WIDTH_80MHZ, CHANNEL_WIDTH_160MHZ or CHANNEL_WIDTH_80MHZ_PLUS_MHZ.
        public int chanelWidth { get; set; }
        // The primary 20 MHz frequency (in MHz) of the channel over which the client is communicating with the access point.
        public int frequency { get; set; }
        // The detected signal level in dBm, also known as the RSSI.
        public int level { get; set; }
        // Indicates Passpoint operator name published by access point.
        public char[] operatorFriendlyName { get; set; }
        // timestamp in microseconds (since boot) when this result was last seen.
        public long timestamp { get; set; }
        // Indicates venue name (such as 'San Francisco Airport') 
        // published by access point; only available on Passpoint network and if published by access point.
        public char[] venueName { get; set; }
    }
}