﻿using System.Web;
using System.Net;
using System.Web.Script.Serialization;
using System.Net.Sockets;
using System;

namespace capstone.Repositories
{
    public class IPTrackRepo
    {

        public dynamic WebAPI()
        {
            string myIP = HttpContext.Current.Request.UserHostAddress;
            string strQuery;
            string key = "776e39e282300a104dc070fa7d5cee59e1000a33aadac6f7cae8dd7e317ea420";
            HttpWebRequest HttpWReq;
            HttpWebResponse HttpWResp;

            strQuery = "http://api.ipinfodb.com/v3/ip-city/?" + "ip=" + myIP + "&key=" + key + "&format=json";

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            HttpWReq = (HttpWebRequest)WebRequest.Create(strQuery);
            HttpWReq.Method = "GET";
            HttpWResp = (HttpWebResponse)HttpWReq.GetResponse();

            System.IO.StreamReader reader = new System.IO.StreamReader(HttpWResp.GetResponseStream());
            string content = reader.ReadToEnd();

            dynamic item = serializer.Deserialize<object>(content);

            return item;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
    }
}