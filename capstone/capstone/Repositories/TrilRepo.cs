﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Math;
using capstone.Models;

namespace capstone.Repositories
{
    public static class TrilRepo
    {
        // the algorithm implementation for python was taken from
        // https://gis.stackexchange.com/questions/66/trilateration-using-3-latitude-longitude-points-and-3-distances
        // and implemented on C# by Mykhailo Tiapkin on 24.09.2018
        public static dynamic Calculate(List<Location> locations, List<ECEH> eceh)
        {
            var productP1P0 = MatrixRepo.Substraction(eceh.ElementAt(1), eceh.ElementAt(0));

            var productP1P0_norm = MatrixRepo.CalculateMatrixNorm(productP1P0);

            var ex = MatrixRepo.Division(productP1P0, productP1P0_norm);

            var productP2P0 = MatrixRepo.Substraction(eceh.ElementAt(2), eceh.ElementAt(0));

            var i = Matrix.Dot(ex, productP2P0);

            var i_ex = MatrixRepo.Multiplication(ex, i);

            // from wikipedia
            // transform to get circle 1 at origin
            // transform to get circle 2 on x axis
            
            //prepare matrices for manipulation
            var productP2P0_i_ex = MatrixRepo.Substraction(productP2P0, i_ex);
            var productP2P0_i_ex_norm = MatrixRepo.CalculateMatrixNorm(productP2P0_i_ex);

            var ey = MatrixRepo.Division(productP2P0_i_ex, productP2P0_i_ex_norm);
            var ez = Matrix.Cross(ex, ey);
            var productP2P1 = MatrixRepo.Substraction(eceh.ElementAt(2), eceh.ElementAt(1));
            var d = MatrixRepo.CalculateMatrixNorm(productP2P1);
            var j = Matrix.Dot(ey, productP2P1);
            // from wikipedia
            // plug and chug using above values
            var x = (Math.Pow(locations.ElementAt(0).Distance, 2) - Math.Pow(locations.ElementAt(1).Distance, 2) + Math.Pow(d, 2)) / (2 * d);
            var y = ((Math.Pow(locations.ElementAt(0).Distance, 2) - Math.Pow(locations.ElementAt(2).Distance, 2) +
                Math.Pow(i, 2) + Math.Pow(j, 2)) / (2 * j)) - ((i / j) * x);
            // only one case shown here
            var z = Math.Sqrt(Math.Pow(locations.ElementAt(0).Distance, 2) - Math.Pow(x, 2) - Math.Pow(y, 2));
            // mulipliying matrices of trilateration points
            var x_ex = MatrixRepo.Multiplication(ex, (float)x);
            var y_ey = MatrixRepo.Multiplication(ey, (float)y);
            var z_ez = MatrixRepo.Multiplication(ez, (float)z);
            // triPt is an array with ECEF x,y,z of trilateration point
            var triPt = MatrixRepo.Addition(eceh[0], x_ex, y_ey, z_ez);

            // convert back to lat/long from ECEF
            // convert to degrees
            var lat = FromRad2Deg((float)Math.Asin(triPt[2] / 6371));
            var lon = FromRad2Deg((float)Math.Atan2(triPt[1], triPt[0]));

            dynamic loc = new Location() { Latitude = lat, Longitude = lon };

            return loc;
        }


        private static float FromDeg2Rad(float par)
        {
            return ((3.14159265359f / 180f) * par);
        }

        private static float FromRad2Deg(float angle)
        {
            return angle * (180f / 3.14159265359f);
        }

        public static ECEH CalculateECEH(Location l)
        {
            var earth_radius = 6371;

            var cos_lat = Math.Cos(FromDeg2Rad(l.Latitude));
            var cos_long = Math.Cos(FromDeg2Rad(l.Longitude));
            var sin_lat = Math.Sin(FromDeg2Rad(l.Latitude));
            var sin_long = Math.Sin(FromDeg2Rad(l.Longitude));


            var vx = earth_radius * cos_lat * cos_long;

            var vy = earth_radius * cos_lat * sin_long;

            var vz = earth_radius * sin_lat;

            return new ECEH() { vx = float.Parse(vx.ToString()), vy = float.Parse(vy.ToString()), vz = float.Parse(vz.ToString()) };
        }
    }
}