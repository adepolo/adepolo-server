﻿using capstone_model;
using System.Collections.Generic;
using System.Linq;

namespace capstone.Repositories
{
    public class Graph
    {
        public Dictionary<string, Dictionary<string, int>> vertices =
            new Dictionary<string, Dictionary<string, int>>();

        public void add_vertex(string name, Dictionary<string, int> edges)
        {
            vertices[name] = edges;
        }

        public List<string> shortest_path(string start, string finish)
        {
            var previous = new Dictionary<string, string>();
            var distances = new Dictionary<string, int>();
            var nodes = new List<string>();

            List<string> path = null;

            foreach (var vertex in vertices)
            {
                if (vertex.Key == start)
                {
                    distances[vertex.Key] = 0;
                }
                else
                {
                    distances[vertex.Key] = int.MaxValue;
                }

                nodes.Add(vertex.Key);
            }

            while (nodes.Count != 0)
            {
                nodes.Sort((x, y) => distances[x] - distances[y]);

                var smallest = nodes[0];
                nodes.Remove(smallest);

                if (smallest == finish)
                {
                    path = new List<string>();
                    while (previous.ContainsKey(smallest))
                    {
                        path.Add(smallest);
                        smallest = previous[smallest];
                    }

                    break;
                }

                if (distances[smallest] == int.MaxValue)
                {
                    break;
                }

                foreach (var neighbor in vertices[smallest])
                {
                    var alt = distances[smallest] + neighbor.Value;
                    if (alt < distances[neighbor.Key])
                    {
                        distances[neighbor.Key] = alt;
                        previous[neighbor.Key] = smallest;
                    }
                }
            }

            return path;
        }

        public Graph()
        {
            using (var db = new adepoloEntities())
            {
                /* List<vertex> data = db.vertices.Where(d => d.bridge_name.Contains("E1") 
                 || d.bridge_name.Contains("G1") || d.bridge_name.Contains("G2")
                 || d.bridge_name.Contains("E2") || d.bridge_name.Contains("G3")
                 || d.bridge_name.Contains("G4") || d.bridge_name.Contains("tramp_center")
                 || d.bridge_name.Contains("Starbucks")).ToList<vertex>();*/
                //|| d.bridge_name.StartsWith("S")) && !d.bridge_name.Equals("Starbucks")
                List<vertex> data = db.vertices.Where(d => d.bridge_name.StartsWith("G")
                 || d.bridge_name.Contains("Starbucks")).ToList<vertex>();
                //List<vertex> data = db.vertices.ToList<vertex>();
                for (int i = 0; i < data.Count; i++)
                {
                    var connections = new Dictionary<string, int>();

                    if (data[i].north_name != null) connections.Add(data[i].north_name, data[i].north_value.Value);
                    if (data[i].east_name != null) connections.Add(data[i].east_name, data[i].east_value.Value);
                    if (data[i].south_name != null) connections.Add(data[i].south_name, data[i].south_value.Value);
                    if (data[i].west_name != null) connections.Add(data[i].west_name, data[i].west_value.Value);

                    add_vertex(data.ElementAt(i).bridge_name, connections);
                }
            }
        }
    }
}