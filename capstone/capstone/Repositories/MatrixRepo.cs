﻿using Accord.Math;
using capstone.Models;

namespace capstone.Repositories
{
    public static class MatrixRepo
    {

        public static float CalculateMatrixNorm(float[] matrix)
        {
            var norm = Norm.Euclidean(matrix);

            return norm;
        }


        public static float[] Substraction(ECEH A, ECEH B)
        {
            float[] product = new float[3];
            product[0] = A.vx - B.vx;
            product[1] = A.vy - B.vy;
            product[2] = A.vz - B.vz;

            return product;
        }

        public static float[] Substraction(float[] A, float[] B)
        {
            float[] product = new float[3];
            product[0] = A[0] - B[0];
            product[1] = A[1] - B[1];
            product[2] = A[2] - B[2];

            return product;
        }

        public static float[] Division(float[] A, float B)
        {
            float[] product = new float[3];
            product[0] = A[0] / B;
            product[1] = A[1] / B;
            product[2] = A[2] / B;

            return product;
        }

        public static float[] Multiplication(float[] A , float B)
        {
            float[] product = new float[3];
            product[0] = A[0] * B;
            product[1] = A[1] * B;
            product[2] = A[2] * B;

            return product;
        }

        public static float[] Addition(ECEH A, float[] B, float[] C, float[] D)
        {
            float[] product = new float[3];
            product[0] = A.vx + B[0] + C[0] + D[0];
            product[1] = A.vy + B[1] + C[1] + D[1];
            product[2] = A.vz + B[2] + C[2] + D[2];

            return product;
        }
    }
}