﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace capstone
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Trilateration",
                url: "tril/{parameters}",
                defaults: new { controller = "Tril", action = "Index"}
            );

            routes.MapRoute(
                name: "Elevation",
                url: "tril/elevation/{parameters}",
                defaults: new { controller = "Tril", action = "Elevation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Map",
                url: "map",
                defaults: new { controller = "Map", action = "Index", id = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "Brid",
                url: "map/brid/{parameters}",
                defaults: new { controller = "Map", action = "Brid", id = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "demo",
                url: "demo/{action}",
                defaults: new { controller = "Demo", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DemoTril",
                url: "demo/tril",
                defaults: new { controller = "Demo", action = "Tril", id = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "Path",
               url: "map/path/{parameters}",
               defaults: new { controller = "Map", action = "Path", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "CurrentLocation",
               url: "home/currentlocation/{parameters}",
               defaults: new { controller = "Home", action = "CurrentLocation", id = UrlParameter.Optional }
           );


            // this section was added in order to suffice the requirements for the IOS Assignment 3 
            routes.MapRoute(
               name: "Assignment3_mykhailo",
               url: "assignment3/mykhailo/{parameters}",
               defaults: new { controller = "Assignment3Controller", action = "Mykhailo", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Assignment3_michael",
               url: "assignment3/michael/{parameters}",
               defaults: new { controller = "Assignment3Controller", action = "Michael", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Assignment3_jason",
               url: "assignment3/jason/{parameters}",
               defaults: new { controller = "Assignment3Controller", action = "Jason", id = UrlParameter.Optional }
            );
        }
    }
}
