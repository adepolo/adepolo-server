﻿using System.Linq;
using System.Web.Mvc;
using capstone_model;

namespace capstone.Controllers
{
    public class Assignment3Controller : Controller
    {
        private adepoloEntities db = new adepoloEntities();
        // GET: Assignment3
        public JsonResult Mykhailo(string param)
        {
            if(param == "Hardware Sales")
            {
                return Json(db.ASSIGNMENT3_MYKHAILO.Select(d => new { hardwareSales = d.hardwareSales,
                consoleName = d.consoleName}), JsonRequestBehavior.AllowGet);
            }else if (param == "Software Sales")
            {
                return Json(db.ASSIGNMENT3_MYKHAILO.Select(d => new {
                    softwareSales = d.softwareSales,
                    consoleName = d.consoleName
                }), JsonRequestBehavior.AllowGet);
            }
            else if (param == "Game Titles")
            {
                return Json(db.ASSIGNMENT3_MYKHAILO.Select(d => new {
                    gameTitles = d.gameTitles,
                    consoleName = d.consoleName
                }), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Michael()
        {
            return Json(db.ASSIGNMENT3_MICHAEL, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Jason()
        {
            return Json(db.ASSIGNMENT3_JASON, JsonRequestBehavior.AllowGet);
        }
    }
}