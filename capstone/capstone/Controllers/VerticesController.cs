﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using capstone_model;

namespace capstone.Controllers
{
    public class VerticesController : Controller
    {
        private adepoloEntities db = new adepoloEntities();

        // GET: Vertices
        public ActionResult Index()
        {
            return View(db.vertices.ToList());
        }

        // GET: Vertices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vertex vertex = db.vertices.Find(id);
            if (vertex == null)
            {
                return HttpNotFound();
            }
            return View(vertex);
        }

        // GET: Vertices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vertices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,bridge_name,north_name,north_value,east_name,east_value,south_name,south_value,west_name,west_value")] vertex vertex)
        {
            if (ModelState.IsValid)
            {
                db.vertices.Add(vertex);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vertex);
        }

        // GET: Vertices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vertex vertex = db.vertices.Find(id);
            if (vertex == null)
            {
                return HttpNotFound();
            }
            return View(vertex);
        }

        // POST: Vertices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,bridge_name,north_name,north_value,east_name,east_value,south_name,south_value,west_name,west_value")] vertex vertex)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vertex).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vertex);
        }

        // GET: Vertices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vertex vertex = db.vertices.Find(id);
            if (vertex == null)
            {
                return HttpNotFound();
            }
            return View(vertex);
        }

        // POST: Vertices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            vertex vertex = db.vertices.Find(id);
            db.vertices.Remove(vertex);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
