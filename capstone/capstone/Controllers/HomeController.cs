﻿using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using Newtonsoft.Json;
using capstone.Models;
using capstone_model;
using System.Linq;
using System;

namespace capstone.Controllers
{
    public class HomeController : Controller
    {
        public JsonResult Index()
        {
            string filePath = Server.MapPath(Url.Content("~/Content/accessPoints.json"));
            var json = LoadJson(filePath);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public List<RoomParameter> LoadJson(string path)
        {
            using (StreamReader r = new StreamReader(path))
            {
                var json = r.ReadToEnd();

                var items = JsonConvert.DeserializeObject<List<RoomParameter>>(json);

                return items;
            }
        }


        [HttpPost]
        public JsonResult CurrentLocation(string mac_1, string mac_2, string mac_3, string mac_4, string mac_5, string mac_6, string mac_7)
        {
            using (var db = new adepoloEntities())
            {
                if(mac_1 != null){

                    try{
                        var access_point = db.AccessPoints.Where(d => d.mac_1 == mac_1 &&
                                                                 d.mac_2 == mac_2 && d.mac_3 == mac_3
                                                                 && d.mac_4 == mac_4 && d.mac_5 == mac_5 &&
                                                                 d.mac_6 == mac_6 && d.mac_7 == mac_7).ToList<AccessPoint>();
                        if (access_point != null)
                        {
                            return Json(access_point.First<AccessPoint>().vertex_name, JsonRequestBehavior.DenyGet);
                        }
                        else
                        {
                            return null;
                        }
                    }catch(Exception ex){
                            return Json(ex.Message);
                    }
                }
                else
                {
                    return null;
                }
            }
        }
    }   
}