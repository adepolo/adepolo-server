﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using capstone_model;

namespace capstone.Controllers
{
    public class AccessPointsController : Controller
    {
        private adepoloEntities db = new adepoloEntities();

        // GET: AccessPoints
        public ActionResult Index()
        {
            return View(db.AccessPoints.ToList());
        }

        // GET: AccessPoints/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccessPoint accessPoint = db.AccessPoints.Find(id);
            if (accessPoint == null)
            {
                return HttpNotFound();
            }
            return View(accessPoint);
        }

        // GET: AccessPoints/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AccessPoints/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,vertex_name,mac_1,mac_2,mac_3,mac_4,mac_5,mac_6,mac_7")] AccessPoint accessPoint)
        {
            if (ModelState.IsValid)
            {
                db.AccessPoints.Add(accessPoint);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(accessPoint);
        }

        // GET: AccessPoints/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccessPoint accessPoint = db.AccessPoints.Find(id);
            if (accessPoint == null)
            {
                return HttpNotFound();
            }
            return View(accessPoint);
        }

        // POST: AccessPoints/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,vertex_name,mac_1,mac_2,mac_3,mac_4,mac_5,mac_6,mac_7")] AccessPoint accessPoint)
        {
            if (ModelState.IsValid)
            {
                db.Entry(accessPoint).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(accessPoint);
        }

        // GET: AccessPoints/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccessPoint accessPoint = db.AccessPoints.Find(id);
            if (accessPoint == null)
            {
                return HttpNotFound();
            }
            return View(accessPoint);
        }

        // POST: AccessPoints/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AccessPoint accessPoint = db.AccessPoints.Find(id);
            db.AccessPoints.Remove(accessPoint);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
