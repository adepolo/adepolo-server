﻿using System.Web.Mvc;
using capstone.Models;
using System.Net;
using System.Web.Script.Serialization;
using capstone_model;
namespace capstone.Controllers
{
    public class TrilController : Controller
    {
        private adepoloEntities db = new adepoloEntities();
        [HttpGet]
        public JsonResult Index()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            /*List<Location> locations = new List<Location>();
             locations.Add(new Location() { Latitude = 37.418436f, Longitude = -121.963477f, Distance = 0.265710701754f });
             locations.Add(new Location() { Latitude = 37.417243f, Longitude = -121.961889f, Distance = 0.234592423446f });
             locations.Add(new Location() { Latitude = 37.418692f, Longitude = -121.960194f, Distance = 0.0548954278262f });
             //[{"Latitude":37.4184341,"Longitude":-121.963478,"Distance":0.2657107},{"Latitude":37.417244,"Longitude":-121.961891,"Distance":0.234592423},{"Latitude":37.4186935,"Longitude":-121.9602,"Distance":0.0548954271}]
             var str = serializer.Serialize(locations);*/
            string param = this.Request.QueryString["json"];
            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            //List<Location> locations1 = serializer.Deserialize<List<Location>>(param);
            //List<Location> locations = 
            //var obj =  new JavaScriptSerializer().Deserialize<List<Location>>(loc);

            //List<Location> locations = new List<Location>();
            /*var eceh = new List<ECEH>();

            eceh.Add(TrilRepo.CalculateECEH(locations1.ElementAt(0)));
            eceh.Add(TrilRepo.CalculateECEH(locations1.ElementAt(1)));
            eceh.Add(TrilRepo.CalculateECEH(locations1.ElementAt(2)));

            dynamic answer = TrilRepo.Calculate(locations1, eceh);*/

            var k = db.vertices;
            
            return Json( k, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Elevation(Location location)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            var req = (HttpWebRequest)WebRequest.Create
                ("https://maps.googleapis.com/maps/api/elevation/json?locations=" + location.Latitude + "," + location.Longitude +
                "&key=AIzaSyB1RkzyUGoovvDZK1-STvnJYZIqM7jePL4");
            req.Method = "GET";
            var res = (HttpWebResponse)req.GetResponse();

            System.IO.StreamReader reader = new System.IO.StreamReader(res.GetResponseStream());
            string content = reader.ReadToEnd();

            dynamic item = serializer.Deserialize<object>(content);
            return Json(item, JsonRequestBehavior.AllowGet);
        }
    }
}