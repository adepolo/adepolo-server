﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Web.Script.Serialization;
using System.Text;
using capstone.Models;
using capstone.Repositories;

namespace capstone.Controllers
{
    public class DemoController : Controller
    {
        // GET: Demo
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Elevation()
        {
            object location = new Location() { Latitude = 39.7391536f, Longitude = -104.9847034f };

            var json = new JavaScriptSerializer().Serialize(location);


            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] data = encoder.GetBytes(json);

            var request = (HttpWebRequest)WebRequest.Create
                ("http://localhost:2008/tril/elevation/");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;
            request.Expect = "application/json";

            request.GetRequestStream().Write(data, 0, data.Length);

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;

            System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
            string content = reader.ReadToEnd();
            dynamic item = new JavaScriptSerializer().Deserialize<object>(content);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Tril()
        {
            var locations = new List<Location>();
            var eceh = new List<ECEH>();

            locations.Add(new Location() { Latitude = 37.418436f, Longitude = -121.963477f, Distance = 0.265710701754f });
            locations.Add(new Location() { Latitude = 37.417243f, Longitude = -121.961889f, Distance = 0.234592423446f });
            locations.Add(new Location() { Latitude = 37.418692f, Longitude = -121.960194f, Distance = 0.0548954278262f });

            eceh.Add(TrilRepo.CalculateECEH(locations.ElementAt(0)));
            eceh.Add(TrilRepo.CalculateECEH(locations.ElementAt(1)));
            eceh.Add(TrilRepo.CalculateECEH(locations.ElementAt(2)));


            return Json(TrilRepo.Calculate(locations, eceh), JsonRequestBehavior.AllowGet);
        }
    }
}