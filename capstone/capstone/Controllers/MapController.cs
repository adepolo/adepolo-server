﻿using System.Web.Mvc;
using capstone.Repositories;
using capstone_model;
using System.Collections.Generic;
using System.Linq;


namespace capstone.Controllers
{
    
    public class MapController : Controller
    {
        Graph g = new Graph();
        // GET: Map
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Brid()
        {
            var start = "G1_exit";
            var dest = "G207";
            var op = g.shortest_path(start, dest);
            op.Add(start);

            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Path(string start, string dest)
        {
            using (var db = new adepoloEntities())
            {
                var op = g.shortest_path(start, dest);
                op.Add(start);

                return Json(op, JsonRequestBehavior.AllowGet);
            }
        }
    }
}