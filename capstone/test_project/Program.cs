﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
//using capstone.Repositories;
using capstone_model;

namespace test_project
{

    class Vertex {
        public string name { get; set; }
        public List<dynamic> distances { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var db = new adepoloEntities();
            Graph g = new Graph();
            List<vertex> data = db.vertices.Where(d => (d.bridge_name.StartsWith("C")
                || d.bridge_name.StartsWith("S") || d.bridge_name.StartsWith("E")
                || d.bridge_name.StartsWith("G") || d.bridge_name.StartsWith("t")))
                    .ToList<vertex>();
            
            var start = "";
            var dest = "";
            var all_passes = "";
            var error = "";
            var pass = "";
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(@"C:\Users\misha\Documents\capstone\server\capstone\capstone\Content\", "file.txt")))
            {

                try
                {
                    for (int i = 0; i < data.Count - 1; i++)
                    {
                        pass = "";
                        for (int j = 0; j < data.Count; j++)
                        {
                            start = data[i].bridge_name;
                            dest = data[j].bridge_name;
                            var op = g.shortest_path(start, dest);
                            op.Add(start);
                            var path = "";
                            foreach (string p in op)
                            {
                                path += p + "...";
                            }
                            //pass += path;
                            outputFile.WriteLine(path);
                           
                            //all_passes += "\n";
                        }

                    }
                    //TestContext.WriteLine("PASSED: \n" + all_passes);
                }
                catch (Exception ex)
                {
                    error += all_passes + "\n\n";
                    error += "ERROR:   \n" + start;
                    error += "";
                    error += dest;
                    error += " | ";
                    outputFile.WriteLine(error);
                    //Console.WriteLine(error);
                    //TestContext.WriteLine(error);
                }
            }
            Console.ReadLine();
        }
    }
    
}





/*var locations = new List<Location>();
            var eceh = new List<ECEH>();
            locations.Add(new Location() { Latitude = 37.418436f, Longitude = -121.963477f, Distance = 0.265710701754f });
            locations.Add(new Location() { Latitude = 37.417243f, Longitude = -121.961889f, Distance = 0.234592423446f });
            locations.Add(new Location() { Latitude = 37.418692f, Longitude = -121.960194f, Distance = 0.0548954278262f });

            eceh.Add(TrilRepo.CalculateECEH(locations.ElementAt(0)));
            eceh.Add(TrilRepo.CalculateECEH(locations.ElementAt(1)));
            eceh.Add(TrilRepo.CalculateECEH(locations.ElementAt(2)));

            TrilRepo.Calculate(locations, eceh);
            float[] A = new float[3];
            float[] B = new float[3];
            float[] C = new float[3];
            float[] Aa = new float[3];
            float[] Bb = new float[3];
            float[] Cc = new float[3];

            // point A 
            A[0] = eceh.ElementAt(0).vx;
            A[1] = eceh.ElementAt(0).vy;
            A[2] = eceh.ElementAt(0).vz;
            // point B
            B[0] = eceh.ElementAt(1).vx;
            B[1] = eceh.ElementAt(1).vy;
            B[2] = eceh.ElementAt(1).vz;
            // point C
            C[0] = eceh.ElementAt(2).vx;
            C[1] = eceh.ElementAt(2).vy;
            C[2] = eceh.ElementAt(2).vz;
            // is a point of origin
            Aa[0] = 0f;
            Aa[1] = 0f;
            Aa[2] = 0f;
            // shifting B point
            Bb[0] = B[0] - A[0];
            Bb[1] = B[1] - A[1];
            Bb[2] = 0f;//B[2] - A[2];
            // shifting C
            Cc[0] = C[0] - A[0];
            Cc[1] = C[1] - A[1];
            Cc[2] = 0f;//C[2] - A[2];




            float tan_of_rotation = B[1] / B[0];
            float angle = (float)Math.Atan(tan_of_rotation);
            Console.WriteLine(angle);
            float x = Bb[0];
            float y = Bb[1];
            Bb[0] = (float)(x * Math.Cos(angle) + y * Math.Sin(angle));
            Bb[1] = (float)(y * Math.Cos(angle) - x * Math.Sin(angle));
            float xb = (float)Math.Sqrt(Bb[0]* Bb[0] + Bb[1] * Bb[1]);
            float yc = (float)Math.Sqrt(Cc[0] * Cc[0] + Cc[1] * Cc[1]);
            Console.WriteLine("X: " + Bb[0] + "; Y: " + Bb[1]);

            float som = (0.265710701754f * 0.265710701754f - 0.234592423446f * 0.234592423446f + xb * xb)/(2* xb);
            float c = (float)Math.Sqrt(0.255f * 0.255f + 0.13836f * 0.13836f);
            float som1 = (0.265710701754f * 0.265710701754f - 0.0548954278262f * 0.0548954278262f + yc * yc) / (2 * yc);
            float k = 0.265710701754f * 0.265710701754f - som * som - som1 * som1;
            float z = (float)Math.Sqrt(k);*/
