CREATE TABLE vertices(
	id int IDENTITY,
	bridge_name VARCHAR(30),
	north_name VARCHAR(30),
	north_value int,
	east_name VARCHAR(30),
	east_value int,
	south_name VARCHAR(30),
	south_value int,
	west_name VARCHAR(30),
	west_value int,
	CONSTRAINT PK_Vertices PRIMARY KEY(id)
);